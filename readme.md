# phory

		     ρρρρρρρ   
		   ρρ    ρρρρ  
		  ρρ       ρρρ 
		  ρρ       ρρρ 
		  ρρ        ρρ 
		  ρρ        ρρ 
		  ρρ       ρρ  
		  ρρρ     ρρ   
		  ρρ ρρρρρ     
		  ρρ           
		  ρρ           
		 ρρρ           
		 ρρρ           

is a python project initializer, which organizes the initial structure and content of the project in different ways according to the required work and impact of the project, this is called project size, for cli.

	positional arguments:
	  proyectName
	optional arguments:
	  -h, --help            show this help message and exit
	  -s SIZE, --size SIZE  scale of proyect (default: medium) small,medium,big,flaskBig,flask.
	  -a AUTHOR, --author AUTHOR
	                        name of who did it
	  -l PROGRAMMINGLANGUAGE, --programmingLanguage PROGRAMMINGLANGUAGE
	                        programming language for init files(defualt: python,only suported now)
### Examples

build file structure of flask

	python phroy.py <name> -s flask -a <author> -l python

build my file system

	python phroy.py <name> -s flaskBig -a <author> -l python

### Screenshots

![tree](https://raw.githubusercontent.com/jero98772/phroy/main/misc/screenshots/tree.png?token=ACZB27P5ELU2V2G3DR7KEUDBIIIPY)

![data](https://raw.githubusercontent.com/jero98772/phroy/main/misc/screenshots/data.png?token=ACZB27P5ELU2V2G3DR7KEUDBIIIPY)

### Installing
**Download repositories**

    git clone https://github.com/jero98772/phory.git

**Run:**  

	python phory.py <proyectName>

### Made for:

create python proyects for github repository automatically, to save time
